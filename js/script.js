/*Создайте метод который будет умножать элементы массива на то число которое будет передавать
пользователь.
Сделайте так, чтобы метод наследовался каждым массивом подобно методу pop().*/

window.addEventListener("DOMContentLoaded", ()=>{
  const input = document.createElement("input"),
  button = document.createElement("button"),
  section = document.createElement("section");

  input.type = "number";
  input.classList.add("input-style");
  button.textContent = "Подсчитать";
  button.classList.add("button-style");

  document.body.append(section);
  section.append(input,button);

  const user = {
    arr: [10,20,32,123,3.2],
    show(){
      button.addEventListener("click", ()=>{
        this.arr.forEach((el)=>{
          button.value = `${input.value}`;
          const p = document.createElement("p"),
          block = document.createElement("div");
          block.classList.add("block");
          p.textContent = button.value * el;
          document.body.append(block);
          block.append(p);
        })
      })
    }
  }
  user.show()

  const user2 = {
    arr: [231,21,321,-33,311,2.1],
  }
  user.show.bind(user2)();
})

/*Сделать слайдер. Слайдер должен переключать картинки каждые 15сек. Также пользователь нажатием по стрелочке в лево и
право может переключать слайды. Реализовать слайдер используя jQuery.
*/

window.addEventListener("DOMContentLoaded", ()=>{
  let position = 0;
  const slider_track = $('.slider-track');

  setInterval(function(){
      position = position + 400;
      if(position > 1200){
          position = 0;
      }
      slider_track.css({
          left: -position + 'px'
      });
  }, 7000);

  const btnNext = $("#btn-next");

  btnNext.css({
      display: "none"
  });

  btnNext.click(()=>{
      position = position + 400;
      if(position > 1200){
          position = 0;
      }
      slider_track.css({
          left: -position + 'px'
      });
  });

  const btnPrev = $("#btn-prev");

  btnPrev.css({
      display: "none"
  });

  btnPrev.click(()=>{
     position = position - 400;
     if(position < 0){
      position = 1200
     }
     slider_track.css({
     left: -position + 'px'
     });
  });

  $(".slider-container").mouseover(()=>{
      btnNext.css({
          display: "block"
      });
      btnPrev.css({
          display: "block"
      });
  });

  $(".slider-container").mouseout(()=>{
      btnNext.css({
          display: "none"
      });
      btnPrev.css({
          display: "none"
      });
  })
})
/*$(document).ready(function() {
 $(".slider").each(function () { // обрабатываем каждый слайдер
  var obj = $(this);
  console.log(obj)
  $(obj).append("<div class='nav'></div>");
  $(obj).find("li").each(function () {
   $(obj).find(".nav").append("<span rel='"+$(this).index()+"'></span>"); // добавляем блок навигации
   $(this).addClass("slider"+$(this).index());
  });
  $(obj).find("span").first().addClass("on"); // делаем активным первый элемент меню
 });
});
function sliderJS (obj, sl) { // slider function
 var ul = $(sl).find("ul"); // находим блок
 var bl = $(sl).find("li.slider"+obj); // находим любой из элементов блока
 var step = $(bl).width(); // ширина объекта
 $(ul).animate({marginLeft: "-"+step*obj}, 500); // 500 это скорость перемотки
}
$(document).on("click", ".slider .nav span", function() { // slider click navigate
 var sl = $(this).closest(".slider"); // находим, в каком блоке был клик
 $(sl).find("span").removeClass("on"); // убираем активный элемент
 $(this).addClass("on"); // делаем активным текущий
 var obj = $(this).attr("rel"); // узнаем его номер
 sliderJS(obj, sl); // слайдим
 return false;
});*/
